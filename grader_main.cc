#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "grader.h"

using namespace std;

inline bool CompareStudent(const pair<string, double>& a,
                           const pair<string, double>& b) {
  return a.second>b.second;
  // 여기에 성적 내림차순으로 정렬되도록 대소비교문 작성.
}

double GetNumberGrade(const string& str) {
  if (str =="A+") return 4.5;
  if (str == "A" || str == "P") return 4.0;
  if (str =="B+") return 3.5;
  if (str == "B") return 3.0;
  if (str =="C+") return 2.5;
  if (str == "C") return 2.0;
  if (str =="D+") return 1.5;
  if (str == "D") return 1.0;
  return 0.0;
}

int main() {
  int num=0;
  vector<Subject*> subject;
  //SubjectPassFail subject1("Seminar", 1, 70);
  //SubjectGrade subject2("C++", 6, 90, 80, 70, 60);
  //SubjectGrade subject3("Calculus", 3, 80, 60, 40, 20);
  //SubjectGrade subject4("Statistics", 2, 80, 70, 60, 50);
  int total_credit=0;
  string cmd;
  vector<pair<string, double> > student_grades;
  while (cmd != "quit") {
    cin >> cmd;
	if(cmd == "subject") {
		int grade, cut1,cut2,cut3,cut4;
		string op,name;
		cin>>name>>grade>>op;
		if(op=="G+"){
			cin>>cut1>>cut2>>cut3>>cut4;
			SubjectGrade2* tmp0 = new SubjectGrade2(name,grade,cut1,cut2,cut3,cut4);
			subject.push_back(tmp0);
			total_credit+=grade;
		}
		if(op=="G"){
			cin>>cut1>>cut2>>cut3>>cut4;
			SubjectGrade* tmp0 = new SubjectGrade(name,grade,cut1,cut2,cut3,cut4);
			subject.push_back(tmp0);
			total_credit+=grade;
		}
		if(op=="PF"){
			cin>>cut1;
			SubjectPassFail* tmp0 = new SubjectPassFail(name,grade,cut1);
			subject.push_back(tmp0);
			total_credit+=grade;
		}
	}
    if (cmd == "eval") {
      string name;
      string tmp;
      vector<string> grade;
      int score;
	  cin>>name;
	  cout<<name<<" ";
	  for(int i=0; i<subject.size(); i++){
		  cin>>score;
		  tmp=subject[i]->GetGrade(score);
		  grade.push_back(tmp);
		  cout<<grade[i]<<" ";
	  }
	  cout<<endl;
	  double aver=0;
      for(int i=0; i<grade.size(); i++){
		  aver+=(subject[i]->credit())*GetNumberGrade(grade[i]);
	  }
	  aver=aver/ total_credit;
      student_grades.push_back(make_pair(name, aver));
    }
  }
  sort(student_grades.begin(), student_grades.end(), CompareStudent);
  for (int i = 0; i < student_grades.size(); ++i) {
    // 여기에서 학점 출력이 소숫점 두자리만 되도록 cout의 멤버함수 호출
	cout.precision(3);
    cout << student_grades[i].first << " " << student_grades[i].second << endl;
  }
  for(int i=0;i<subject.size();i++) delete subject[i];
  return 0;
}
