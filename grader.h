#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Subject {

 public:

  Subject(const string& name, int credit)

      : name_(name), credit_(credit) {}

  virtual ~Subject() {}


  const string& name() const { return name_; }

  int credit() const { return credit_; }

  virtual string GetGrade(int score) const = 0;

 private:

  string name_;

  int credit_;

};


// 성적을 Pass / Fail 로 구분하여 출력해주는 클래스.

// 성적이 pass_score보다 같거나 높으면 "P", 아니면 "F"를 리턴.

class SubjectPassFail : public Subject {

 public:

  SubjectPassFail(const string& name, int credit, int pass_score)
	  :Subject(name,credit)
  {
	  pass_score_=pass_score;

  }

  virtual ~SubjectPassFail() {

  }

  virtual string GetGrade(int score) const {
	  if(pass_score_<=score){return "P";}
	  return "F";
  }

 private:

  int pass_score_;

};


// 성적을 A, B, C, D, F 로 구분하여 출력해주는 클래스.

// 성적이 속하는 구간에 따라

// 100 >= "A" >= cutA > "B" >= cutB > "C" >= cutC > "D" >= cutD > "F".

class SubjectGrade : public Subject {

 public:

  SubjectGrade(const string& name, int credit,

               int cutA, int cutB, int cutC, int cutD)
	  :Subject(name,credit)
  {
				   cutA_=cutA; cutB_=cutB; cutC_=cutC; cutD_=cutD;
  }

  virtual ~SubjectGrade() {
	  
  }


  virtual string GetGrade(int score) const {
	  if(score>=cutA_) return "A";
	  else if(score>=cutB_) return "B";
	  else if(score>=cutC_) return "C";
	  else if(score>=cutD_) return "D";
	  else return "F";
  }


 private:

  int cutA_, cutB_, cutC_, cutD_;

};

class SubjectGrade2 : public Subject {

 public:

  SubjectGrade2(const string& name, int credit,

               int cutA, int cutB, int cutC, int cutD)
	  :Subject(name,credit)
  {
		cutAp_=(100+cutA)/2; 
	        cutA_=cutA; cutBp_=(cutA+cutB)/2; cutB_=cutB; cutCp_=(cutB+cutC)/2; cutC_=cutC;  cutDp_=(cutC+cutD)/2;  cutD_=cutD;
  }

  virtual ~SubjectGrade2() {
	  
  }


  virtual string GetGrade(int score) const {
	  if(score>=cutAp_) return "A+";
	  else if(score>=cutA_) return "A";
	  else if(score>=cutBp_) return "B+";
	  else if(score>=cutB_) return "B";
	  else if(score>=cutCp_) return "C+";
	  else if(score>=cutC_) return "C";
	  else if(score>=cutDp_) return "D+";
	  else if(score>=cutD_) return "D";
	  else return "F";
  }


 private:

  int cutAp_,cutA_,cutBp_, cutB_,cutCp_,cutC_, cutD_,cutDp_;

};
