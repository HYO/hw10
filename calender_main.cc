#include <iostream>
#include <string>
#include "calender.h"
#include <stdlib.h>
#include <vector>
int main() {
  Date date; 
  string cmd;
  while (cmd != "quit") {
    cin >> cmd;
    try {
		  if (cmd == "set") {
			  int year,month,day,tmp;
			  string str="";
			  cin>>str;
			  string str2=str;
		 	  year=atoi(str.substr(0,str.find(".")).c_str());
			  str=str.substr(str.find(".")+1);
		 	  month=atoi(str.substr(0,str.find(".")).c_str());
			  str=str.substr(str.find(".")+1);
			  day=atoi(str.substr(0,str.find(".")).c_str());
			  bool check;
			  check=date.SetDate(year,month,day);
			  if(check==false)throw InvalidDateException(str2);
		  }
		  if (cmd == "next_day") {
			  date.NextDay(1);
		  }
		  if (cmd == "next") {
			  int n;
			  cin>>n;
			  date.NextDay(n);
		  }
		  if(cmd!="quit")cout<<date.year()<<"."<<date.month()<<"."<<date.day()<<endl;
		} catch (InvalidDateException& e) {
			cout<<"Invalid date: "<<e.input_date<<endl;
		}
	}
	return 0;
}
