#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Date {

 public:

  Date(){

  }

  Date(int year, int month, int day){
	  year_=year;
	  month_=month;
	  day_=day;
  }

  void NextDay(int n = 1) {
	  day_+=n;
	  if((month_%7)%2==1)if(day_>31){ month_++; day_-=31;}
	  else if(month_==2){
		  if(GetDaysInYear(year_)==366){
			  if(day_>29){ month_++; day_-=29;}
		  }
		  else{
			  if(day_>28){ month_++; day_-=28;}
		  }
	  }
	  else if((month_%7)%2==0)if(day_>30){ month_++; day_-=30;}
	  if(month_>12) { year_++; month_-=12;}
	  if(day_<=0) {month_--;
	  if(month_<=0) {year_--; month_+=12;}
	  if((month_%7)%2==1) day_+=31;
	  else if(month_==2){
		  if(GetDaysInYear(year_)==366){day_+=29;
		  }
		  else{day_+=28;
		  }
	  }
	  else if((month_%7)%2==0)day_+=30;
	  }

	  
  }

  bool SetDate(int year, int month, int day) {
	  if(month>12) return false;
	  if(month<=0) return false;
	  if(day<=0) return false;
	  if((month%7)%2==1){if(day>31)return false;}
	  else if(month==2){
		  if(GetDaysInYear(year)==366){
			  if(day>29)return false;
		  }
		  else{
			  if(day>28)return false;
		  }
	  }
	  else if((month%7)%2==0)if(day>30)return false;
	  year_=year;
	  month_=month;
	  day_=day;
	  return true;
  }

  int year() const { return year_; }

  int month() const { return month_;}

  int day() const { return day_; }


 private:

  // 윤년을 판단하여 주어진 연도에 해당하는 날짜 수(365 또는 366)를 리턴.

  static int GetDaysInYear(int year) {
	  if(year%4==0&&year%100!=0) return 366;
	  if(year%400==0) return 366;
	  return 365;
  }

  // 해당 날짜가 해당 연도의 처음(1월 1일)부터 며칠째인지를 계산.

  static int ComputeDaysFromYearStart(int year, int month, int day) {
	  int date=day-1;
	  while(month>=0) {
	  if((month%7)%2==1){ 
		 date+=31;
		 month--;
	  }
	  if(month==2){
		  if(GetDaysInYear(year)==366){
			  month--;
			  date+=29;
		  }
		  else{
			  month--;
			  date+=28;
		  }
	  }
	  else if((month%7)%2==0){
		  month--;
		  date+=30;
	  }	  
	  }
	  return date;
  }

  int year_, month_, day_;

};


struct InvalidDateException {

  string input_date;

  InvalidDateException(const string& str) : input_date(str) {}

};

