#include<iostream>
#include<string>
#include<vector>
#include<fstream>
using namespace std;

class Account { 

public:

  Account(const string& name, unsigned int balance, double interest_rate) {
	  name_=name;
	  balance_=balance;
	  interest_rate_=interest_rate;
  }

  virtual ~Account(){};
  
  void Deposit(unsigned int amount) {
	  balance_+=amount;
  }

  bool Withdraw(unsigned int amount) {
	  balance_-=amount;
  }

  virtual unsigned int ComputeExpectedBalance(

      unsigned int n_years_later) const {
		  unsigned int amount=balance_;
		  amount*=(1+n_years_later*(interest_rate_));
		  return amount;
  }

  virtual const char* type() const { return "checking"; }

  const unsigned int& balance() const { return balance_; }

  const string& name() const { return name_; }

  const double& interest_rate() const { return interest_rate_; }

        

 protected:

  string name_;

  unsigned int balance_;


  double interest_rate_;  // 기본 계좌는 단리로 계산.

};           

             

class SavingAccount : public Account {

 public:

  SavingAccount(const string& name, int balance,

                double interest_rate) : 
					Account::Account(name,balance,interest_rate)
 { }

  virtual ~SavingAccount(){};

      

  virtual const char* type() const { return "saving"; }

  // 이 타입의 계좌는 복리로 계산.

  virtual unsigned int ComputeExpectedBalance(

      unsigned int n_years_later) const{
		  
		  double amount=balance_;
		  for(int i=0; i<n_years_later; i++) {
			  amount*=(1+interest_rate_);
		  }
		  return (unsigned int)amount;
  }


};    

      

Account* CreateAccount(const string& type,

    const string& name, unsigned int balance, double interest_rate){
	if(type=="checking") { Account* tmp= new Account(name,balance,interest_rate);
	return tmp; }
	if(type=="saving") {SavingAccount* tmp= new SavingAccount(name,balance,interest_rate);
	return tmp;}
}	

      

bool SaveAccounts(const vector<Account*>& accounts, const string& filename){
	ofstream fp;
	fp.open(filename.c_str());
	for(int i=0; i<accounts.size(); i++)
	fp << accounts[i]->name()<<" "<<accounts[i]->type()<<" "<<accounts[i]->balance()<<" "<<accounts[i]->interest_rate();
	fp.close();
}

bool LoadAccounts(const string& filename, vector<Account*>* accounts){
	ifstream fp;
	fp.open(filename.c_str());
	string name,type;
	int bal;
	double inter;
	while(!fp.eof()) {
		fp>>name>>type>>bal>>inter;
		accounts->push_back(CreateAccount(type,name,bal,inter));
	}
	fp.close();
}

